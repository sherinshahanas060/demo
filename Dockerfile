# FROM : It is for specifying the base image
FROM centos

MAINTAINER Sherin Shahanas
LABEL Description="Centos with Tomcat,python and MongoDB "

WORKDIR /opt
RUN yum -y update
#Install Java 1.8
RUN yum -y install java-1.8.0-openjdk.x86_64 java-1.8.0-openjdk-devel.x86_64
RUN yum -y install wget
#Create user and group  for tomcat
RUN groupadd tomcat && useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat


#Download tomcat package and configure it
RUN cd /opt && \
    wget http://mirror.fibergrid.in/apache/tomcat/tomcat-7/v7.0.77/bin/apache-tomcat-7.0.77.tar.gz && \
    tar -xzvf apache-tomcat-7.0.77.tar.gz && \
    mv apache-tomcat-7.0.77/* tomcat/ && \
    chown -hR tomcat:tomcat tomcat
	
#Setup Java home environment variable	
ENV JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.131-2.b11.el7_3.x86_64"
ENV PATH=$JAVA_HOME/bin:$PATH
#start tomcat
CMD /opt/tomcat/bin/catalina.sh start                   
EXPOSE 8080
################################################################################################################
#Install Python 2.7
RUN     yum -y groupinstall "Development tools"
RUN     yum -y install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel
RUN     cd /opt && \
        wget --no-check-certificate https://www.python.org/ftp/python/2.7.6/Python-2.7.6.tar.xz && \
        tar xf Python-2.7.6.tar.xz && \
        cd Python-2.7.6 && \
        ./configure --prefix=/usr/local && \
        make && \
        make altinstall
################################################################################################################
#Install MongoDB

RUN echo -e "[mongodb]\nname=MongoDB Repository\nbaseurl=http://downloads-distro.mongodb.org/repo/redhat/os/`uname -m`/\ngpgcheck=0\nenabled=1" > /etc/yum.repos.d/mongodb.repo                 # Craete a repo for MongoDB
RUN yum -y install mongodb-org-server            # Install Mongodb Server
RUN mkdir -p /data/db
EXPOSE 27017
##################################################################################################################


ENTRYPOINT ["/opt/tomcat/bin/catalina.sh", "run"]

#########################################################################################################
#           Use below commands to build and start the image from the docker file			#
# 	    docker build -t <tagname> <path to Docker file>						#
#	    Ex: docker build -t "sherin/final" .							#
#	    docker run -it <port mapping> <image name> <command>					#
#   	    Ex:docker run -it -p 80:8080 -p 27017:27017 sherin/final /bin/bash 				#
#########################################################################################################
