#!/bin/bash
#This script will take server names in argument line sepprated by comma and execute the command provided by user in all servers mentioned
#Assumes server has password less login and all server use the same user name to login
#Author : Sherin Shahanas
#Mail : sherin.shahanas@hotmail.com
list=$1 #get all server list provided in command line sepprated by comma
count=`echo $list | grep -o "\," | wc -l` #number of servers 
total=$(($count + 1))
#providing the user prompt for entering the command 
echo "Enter the command to execute ::"
read command
var=1
while [ $var -le $total ]
do
  echo Loop $var
  hostname=`echo $list |awk -F"," '{print $'$var'}'`
  echo "Executing command in the Server :: $hostname"
  ssh  root@$hostname $command #Please edit the user name that you want use.
  var=$(($var + 1))
done
